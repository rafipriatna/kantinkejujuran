# Kantin Kejujuran
Aplikasi berbasis website yang diperuntukan untuk memenuhi persyaratan COMPFEST Academy Software Engineer. [Demo Aplikasi](http://kantin.rafipriatna.id/)

## Fitur
Fitur-fitur yang ada pada aplikasi ini:
1. Dagangan: Semua pengguna dapat menjual dan membeli dagangan. Tetapi penjual tidak dapat membeli dagangannya sendiri.
2. Hasil dagangan akan masuk ke `Balance Box`. Balance Box dapat ditambah dan ditarik oleh siapapun yang sudah login.
3. Student ID: Student ID pada palikasi diverifikasi dengan rumus `4 + 5 + 6 = 15` berarti Student ID nya `45615`.
4. Edit Profil: Semua pengguna dapat mengubah nama dan kata sandi.

## Installasi
Berikut ini adalah panduan memasang aplikasi.

### 1. Pasang langsung di mesin/komputer

#### Persyaratan Sistem
- PHP versi >= 8.1
- Composer >= 2.3.5
- Nodejs (Recommended LTS >= 16.14.2)
- MySQL

Pastikan sudah terpasang composer. Jika belum, silakan kunjungi [Composer Download](https://getcomposer.org/download/) untuk melihat panduan install composer.
```bash
# Clone repo ini dan install
git clone https://gitlab.com/rafipriatna/kantinkejujuran
cd kantinkejujuran
composer update
npm install
```
Duplicate .env.example ke .env
```bash
cp .env.example .env
```
Lalu ubah pengaturan database di .env

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nama_database
DB_USERNAME=user
DB_PASSWORD=password_user
```
Lalu migrasi database dan buat linked storage
```bash
# Migrate database
php artisan migrate

# Buat linked storage
php artisan storage:link
```

Jalankan aplikasi
```
php artisan serve
```

Selesai. Aplikasi siap digunakan

### 2. Pasang menggunakan Sail
Sail adalah CLI yang disediakan oleh Laravel untuk membuat _Docker Development Environment_. 
```bash
composer update
npm install

# Menjalankan aplikasi
./vendor/bin/sail up -d

./vendor/bin/sail artisan migrate
./vendor/bin/sail artisan storage:link
```

Dokumentasi tentang Sail dapat diakses melalui [Laravel Sail](https://laravel.com/docs/9.x/sail).

## Testing
Juga tersedia PHPUnit yang dapat dijalankan dengan menggunakan perintah
```bash
./vendor/bin/phpunit
```
Pastikan aplikasi sudah berjalan dan sudah terhubung ke basis data.
Nama basis data yang digunakan untuk testing dapat diubah pada file `phpunit.xml`
```bash
<php>
    <env name="APP_ENV" value="testing"/>
    <env name="BCRYPT_ROUNDS" value="4"/>
    <env name="CACHE_DRIVER" value="array"/>
    <env name="DB_DATABASE" value="kantin_testing"/>
    <env name="MAIL_MAILER" value="array"/>
    <env name="QUEUE_CONNECTION" value="sync"/>
    <env name="SESSION_DRIVER" value="array"/>
    <env name="TELESCOPE_ENABLED" value="false"/>
</php>
```

## Lisensi
Aplikasi ini berada di bawah lisensi [MIT LICENSE](LICENSE)
