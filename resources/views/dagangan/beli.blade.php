@extends('layout.app')
@section('title', $title)

@section('content')
    <nav class="font-bold mt-8 cursor-default" aria-label="Breadcrumb">
        <ol class="list-none p-0 inline-flex">
            <li class="flex items-center text-gray-500">
                Dagangan
                <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                    <path
                        d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
                </svg>
            </li>
            <li class="flex items-center text-gray-500">
                {{ $product->product_category }}
                <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                    <path
                        d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
                </svg>
            </li>
            <li>{{ $product->product_name }}</li>
        </ol>
    </nav>

    <div class="flex w-full h-full mx-auto items-left justify-center py-10">
        <div class="w-full sm:w-7/12 md:w-8/12 lg:w-9/12 xl:w-7/12">
            <div class="flex items-center space-x-4"><a href="/u/harrishash"
                    class="flex-shrink-0 w-20 h-20 overflow-hidden rounded-md "><img
                        src="{{ Storage::url('produk/' . $product->product_image) }}"
                        class="object-cover w-full h-full"></a>
                <div class="flex flex-col space-y-1 font-semibold text-gray-800 transition-colors duration-300 text-2xl">
                    {{ $product->product_name }}
                    <span class="text-sm font-normal my-1">
                        {{ $product->product_price }}
                    </span>
                    <span class="text-sm font-normal my-1">
                        {{ $product->product_category }}
                    </span>
                </div>
            </div>

            <div class="my-10">

                <div class="mb-5">
                    <label>Quantity</label>
                    <input type="number" id="quantity"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        placeholder="1" name="amount" required>
                </div>

                <table>
                    <tr>
                        <th>
                            Total Bayar:
                        </th>
                        <td>
                            <span id="totalBayar">Rp 0, 00</span>
                        </td>
                    </tr>
                </table>

                <div class="my-5">
                    <label>Uang yang dibayar <code class="block text-red-400">Uang akan masuk ke Canteen Balance Box.</code></label>
                    <input type="number"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        placeholder="0" id="paid" required>
                </div>
            </div>

            <button onclick="beliDagangan()" id="tombolBeli"
                class="float-right text-white font-medium text-lg px-3 py-3 rounded-lg flex bg-champ-green bg-champ-green transition ease-out duration-200 hover:bg-opacity-80">
                Bayar
            </button>

        </div>
    </div>
@endsection

@section('js')
    <script>
        const stock = {{ $product->product_stock }}
        const hargaFormatted = '{{ $product->product_price }}'
        const harga = parseInt(hargaFormatted.replace(/\D/g, ''))

        let quantity = 0
        $('#quantity').keyup(function() {
            let isi = parseInt(this.value.replace(/\D/g, ''))

            if (isi < 0) {
                $('#quantity').val(Math.abs(isi))
            }

            if (isi >= stock) {
                isi = stock
                $('#quantity').val(stock)
            }

            quantity = parseInt(isi)
            let totalBayar = harga * parseInt(isi) || 0
            $('#totalBayar').html(formatRupiah(totalBayar))
        })

        function beliDagangan() {
            $('#tombolBeli').prop('disabled', true);
            const paid = $('#paid').val()

            if (quantity < 1) {
                Swal.fire(
                    'Error',
                    'Harap mengisi isian quantity',
                    'error'
                )
                return
            }

            $.ajax({
                type: 'POST',
                url: '{{ route('prosesBeliDagangan', $product->id) }}',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'quantity': quantity,
                    'paid': paid
                },
                success: function(res) {
                    let status = 'success'
                    if (res.title.includes('gagal')) {
                        status = 'error'
                    }
                    Swal.fire(
                        res.title,
                        res.message,
                        status
                    ).then(() => {
                        if (status !== 'error') window.location.href = "{{ route('beranda') }}";
                    })

                },
                error: function(xhr, status, err) {
                    const response = JSON.parse(xhr.responseText)
                    Swal.fire(
                        status,
                        response.message,
                        'error'
                    )
                }
            });

            $('#tombolBeli').prop('disabled', false);
        }

        function formatRupiah(uang) {
            return new Intl.NumberFormat('id-ID', {
                style: 'currency',
                currency: 'IDR'
            }).format(uang)
        }
    </script>
@endsection
