@extends('layout.app')
@section('title', $title)

@section('content')
    <section class="lg:pt-20 pt-10 mb-10">
        <div class="mb-10 text-5xl font-bold leading-tight text-center heading lg:leading-snug font-display">
            {{ $title }}
        </div>
    </section>

    <section>
        <div class="w-full col-span-2 mt-16 md:col-span-1 lg:mt-0">

            <div class="my-2">
                <select class="lg:mx-5 lg:my-0 lg:w-auto w-full rounded-md inline-flex items-center border border-gray-400 my-2 py-2 px-2"
                    onchange="location = this.value;">
                    @foreach ($productSorting as $sortingName => $sorting)
                        <option {{ $sortBy == $sortingName ? 'selected' : '' }} value="{{ $sorting[1] }}">{{ $sorting[0] }}</option>
                    @endforeach
                </select>
                
                <select class="lg:w-auto w-full rounded-md inline-flex items-center border border-gray-400 my-2 py-2 px-2"
                    onchange="location = this.value;">
                    @foreach ($categoriesSorting as $sortingName => $sorting)
                        <option {{ $sortCategory == $sortingName ? 'selected' : '' }} value="{{ $sorting[1] }}">{{ $sorting[0] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="flex flex-wrap">
                @if (count($products) < 1)
                    <p class="mx-auto">Belum ada produk</p>
                @else
                    @foreach ($products as $product)
                        <a href="{{ route('detailDagangan', $product->product_slug) }}">
                            <div
                                class="bg-white max-w-xs cursor-pointer overflow-hidden transform hover:scale-105 duration-500 rounded-lg m-5 hover:shadow-lg my-10 border hover:border-none">
                                <div class="relative h-[350px] m-5">
                                    <div class="top-row">
                                        <div class="image-placeholder w-[268px] h-[190px]">
                                            <img src="{{ Storage::url('produk/' . $product->product_image) }}"
                                                alt="card-image" class="object-cover w-full rounded-lg h-48" />
                                        </div>
                                        <div class="mt-6 text-2xl">
                                            {{ $product->product_name }}
                                        </div>
                                        <p class="text-base font-bold leading-7 text-gray-6">
                                            {{ $product->product_price }}
                                        </p>
                                    </div>

                                    <div class="absolute bottom-0 w-full bottom-row justify-start">
                                        <p class="inline-flex justify-start w-full text-gray-700">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                            </svg>
                                            <span>{{ $product->student->name }}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection
