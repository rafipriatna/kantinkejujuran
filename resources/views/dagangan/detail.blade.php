@extends('layout.app')
@section('title', $title)

@section('content')
    <nav class="font-bold my-8 cursor-default" aria-label="Breadcrumb">
        <ol class="list-none p-0 inline-flex">
            <li class="flex items-center text-gray-500">
                Dagangan
                <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                    <path
                        d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
                </svg>
            </li>
            <li class="flex items-center text-gray-500">
                {{ $product->product_category }}
                <svg class="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                    <path
                        d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
                </svg>
            </li>
            <li>{{ $product->product_name }}</li>
        </ol>
    </nav>

    <section>
        <div class="flex flex-col md:flex-row gap-8">
            <div class="flex-auto w-full lg:w-3/5">
                <img src="{{ Storage::url('produk/' . $product->product_image) }}"
                    class="w-full h-96 rounded-lg object-cover">

                <div class="flex-auto w-full mt-10">
                    <h1 class="text-xl font-semibold">{{ $product->product_name }}</h1>
                    <p class="text-gray-400 text-lg mt-1">
                        {{ $product->product_category }}
                    </p>

                    <div class="mt-5 text-justify text-lg">
                        {!! $product->product_description !!}
                    </div>
                </div>
            </div>

            <div class="flex-auto sm:w-1/5 bg-white md:h-screen top-5 sm:h-auto">
                <div class="mb-4">
                    <h2 class="inline-flex justify-start items-center w-full text-gray-700 font-semibold">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                        </svg>

                        Penjual
                    </h2>
                    <p class="text-gray-400">
                        {{ $product->student->name }}
                    </p>
                </div>
                <div class="mb-4">
                    <h2 class="inline-flex justify-start items-center w-full text-gray-700 font-semibold">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>

                        Harga
                    </h2>
                    <p class="text-gray-400">
                        {{ $product->product_price }}
                    </p>
                </div>
                <div class="mb-4">
                    <h2 class="inline-flex justify-start items-center w-full text-gray-700 font-semibold">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M5 8h14M5 8a2 2 0 110-4h14a2 2 0 110 4M5 8v10a2 2 0 002 2h10a2 2 0 002-2V8m-9 4h4" />
                        </svg>
                        Stok
                    </h2>
                    <p class="text-gray-400">
                        {{ $product->product_stock }}
                    </p>
                </div>

                <a href="{{ route('beliDagangan', $product->product_slug) }}"
                    class="my-4 bg-champ-green text-white w-full font-bold py-2 px-4 rounded inline-flex items-center justify-center transition duration-500 hover:bg-opacity-80">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                    </svg>
                    <span>Beli</span>
                </a>
            </div>

        </div>
    </section>

@endsection
