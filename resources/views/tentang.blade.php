@extends('layout.app')
@section('title', $title)

@section('content')
    <section class="lg:pt-20 pt-10 mb-10">
        <div class="mb-10 text-5xl font-bold leading-tight text-center heading lg:leading-snug font-display">
            {{ $title }}
        </div>
    </section>

    <section>
        <p>
            Kantin Kejujuran SD Sea Sentosa adalah tempat membeli segala macam dagangan yang dijual oleh siswa untuk siswa.
        </p>
        <p>
            Aplikasi ini tidak berafiliasi dengan SD Sea Sentosa, melainkan hanya untuk memenuhi persyaratan COMPFEST Academy Software Engineer.
        </p>
    </section>
@endsection