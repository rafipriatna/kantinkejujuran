<nav class="lg:pt-7 pt-5 z-50">
    <div class="flex flex-col w-full lg:flex-row lg:items-center gap-5 divide-gray-700 lg:divide-x">
        <div class="flex items-center justify-between flex-none">
            <!-- LOGO -->
            <div class="text-xl font-bold">KJ</div>
            <!-- RESPONSIVE NAVBAR BUTTON TOGGLER -->
            <div>
                <button class="block p-1 outline-none lg:hidden mobile-menu-button" data-target="#navigation">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-7 h-7" x-show="!showMenu" fill="none"
                        viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 8h16M4 16h16">
                        </path>
                        <path stroke-linecap="round" class="hidden w-7 h-7" stroke-linejoin="round" stroke-width="2"
                            d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
        <div class="flex hidden w-full mx-auto mobile-menu lg:block" id="navigation">
            <div class="flex flex-col items-baseline justify-between mx-auto mt-6 lg:flex-row lg:items-center lg:mt-0">
                <div class="flex flex-col w-full text-base font-normal lg:flex-row lg:w-max lg:pl-4">
                    <a href="{{ route('beranda') }}"
                        class="py-3 px-4 mx-2 hover:text-champ-green {{ Request::is('/') ? 'font-bold text-champ-green' : '' }}">Beranda</a>
                    <a href="{{ route('tentang') }}"
                        class="py-3 px-4 mx-2 hover:text-champ-green {{ Request::is('tentang') ? 'font-bold text-champ-green' : '' }}">Tentang</a>
                    <a href="{{ route('dagangan') }}"
                        class="py-3 px-4 mx-2 hover:text-champ-green {{ Request::is('dagangan') ? 'font-bold text-champ-green' : '' }}">Dagangan</a>
                </div>
                <div
                    class="flex flex-col lg:flex-row inline-flex gap-4 lg:gap-7 w-full px-3 mt-4 lg:mt-0 lg:w-max lg:px-0">
                    @if (!Auth::check())
                        <a href="{{ route('masuk') }}"
                            class="w-full lg:w-auto px-5 py-3 text-center rounded-lg flex lg:mx-auto transition ease-out duration-200 hover:bg-champ-green hover:bg-opacity-80 hover:text-white">
                            <span class="text-base w-full font-semibold">Masuk</span>
                        </a>
                        <a href="{{ route('daftar') }}"
                            class="w-full lg:w-auto px-5 py-3 text-center rounded-lg flex bg-champ-green lg:mx-auto bg-champ-green transition ease-out duration-200 hover:bg-opacity-80">
                            <span class="text-base w-full font-semibold text-white">Daftar</span>
                        </a>
                    @else
                        <div x-data="{ dropdownMenu: false }" class="relative">
                            <!-- Dropdown toggle button -->
                            <button type="button" @click="dropdownMenu = !dropdownMenu"
                                @click.away="dropdownMenu = false"
                                class="ml-3 inline-flex items-center w-full text-gray-700 font-semibold">
                                Halo, {{ Auth()->user()->name }}
                                <template x-if="!dropdownMenu">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="-mr-1 ml-2 h-5 w-5" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                                    </svg>
                                    </svg>
                                </template>
                                <template x-if="dropdownMenu">
                                    <svg x-if="dropdownMenu == false" xmlns="http://www.w3.org/2000/svg"
                                        class="-mr-1 ml-2 h-5 w-5" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" />
                                    </svg>
                                </template>
                            </button>
                            <!-- Dropdown list -->
                            <div x-show="dropdownMenu"
                                class="absolute right-0 py-2 mt-2 bg-white bg-gray-100 rounded-md shadow-xl w-44 z-50">
                                <div class="block px-4 py-2 text-sm text-champ-green">
                                    Balance Box:
                                    <span class="block">
                                        {{ Auth::user()->canteen_balance_box }}
                                    </span>
                                </div>
                                <hr>
                                <a href="{{ route('balanceBox') }}"
                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-champ-green hover:text-white">
                                    Balance Box
                                </a>
                                <a href="{{ route('profil') }}"
                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-champ-green hover:text-white">
                                    Profil
                                </a>
                                <a href="{{ route('daganganku') }}"
                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-champ-green hover:text-white">
                                    Daganganku
                                </a>
                                <a href="{{ route('keluar') }}"
                                    class="block px-4 py-2 text-sm text-gray-700 hover:bg-champ-green hover:text-white">
                                    Keluar
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</nav>
