@include('layout.header')

<body class="bg-gray-100">
    <main class="font-be-vietnam text-gray-800 h-auto px-4 mx-auto max-w-screen-xl lg:px-24 bg-white min-h-screen">
        @include('layout.navbar')
        @yield('content')
        @include('layout.footer')
    </main>
</body>

</html>