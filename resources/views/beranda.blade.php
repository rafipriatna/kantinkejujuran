@extends('layout.app')
@section('title', $title)

@section('content')
<section class="lg:py-20 py-10">
    <div class="grid grid-cols-12 items-center justify-center wrap">
        <div class="max-w-xl col-span-12 md:col-span-5 lg:col-span-6">
            <div>
                <div class="headline font-bold text-4xl lg:text-5xl leading-normal lg:leading-snug">Kantin Kejujuran SD Sea Sentosa</div>
                <div class="mt-5 mb-9">
                    <p class="font-normal text-sm lg:text-base leading-7">
                        Di sini kamu bisa beli jajanan tanpa diawasi
                        <br class="d-none lg:d-block">
                        Kamu juga bisa berjualan di sini.
                    </p>
                </div>
                <div class="flex inline-flex items-center gap-7">
                    <a href="{{ route('dagangan') }}" class="w-full lg:w-auto px-5 py-3 text-center rounded-lg flex bg-champ-green lg:mx-auto bg-champ-green transition ease-out duration-200 hover:bg-opacity-80">
                        <span class="text-base text-center font-semibold text-white">
                            Lihat Dagangan
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-span-12 md:col-span-7 lg:col-span-6 pointer-events-none	hidden md:block">
            <img src="{{ asset('img/Tasting.png') }}" alt="images" class="object-cover" />
        </div>
    </div>
</section>
@endsection