@extends('layout.app')
@section('title', $title)

@section('content')
<section>
    <div class="content-3-5">
        <div class="flex w-full h-full mx-auto items-left justify-center py-20">
            <div class="w-full sm:w-7/12 md:w-8/12 lg:w-9/12 xl:w-7/12">

                @if (\Session::has('success'))
                <div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800" role="alert">
                    {!! \Session::get('success') !!}
                </div>
                @endif

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert">
                    {{ $error }}
                </div>
                @endforeach
                @endif

                <h3 class="text-3xl font-semibold mb-3">Daftar akun baru</h3>
                <p class="caption leading-7 text-sm">
                    Buat akun baru untuk membeli jajanan di Kantin Kejujuran SD Sea Sentosa.
                </p>
                <form class="mt-6" action="{{ route('prosesDaftar') }}" method="POST">
                    @csrf
                    <div class="mb-4">
                        <label class="block text-lg font-medium text-label">Student ID</label>
                        <div class="flex w-full px-5 py-4 mt-3 text-base font-light rounded-xl input">
                            <svg xmlns="http://www.w3.org/2000/svg" class="mr-4 text-gray-400 w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M10 6H5a2 2 0 00-2 2v9a2 2 0 002 2h14a2 2 0 002-2V8a2 2 0 00-2-2h-5m-4 0V5a2 2 0 114 0v1m-4 0a2 2 0 104 0m-5 8a2 2 0 100-4 2 2 0 000 4zm0 0c1.306 0 2.417.835 2.83 2M9 14a3.001 3.001 0 00-2.83 2M15 11h3m-3 4h2" />
                            </svg>
                            <input type="text" name="student_id" placeholder="45615" class="w-full focus:outline-none text-base font-light" value="{{old('student_id')}}" autocomplete required />
                        </div>
                    </div>
                    <div class="mb-4">
                        <label class="block text-lg font-medium text-label">Nama Lengkap</label>
                        <div class="flex w-full px-5 py-4 mt-3 text-base font-light rounded-xl input">
                            <svg class="mr-4 text-gray-400 w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                            </svg>
                            <input type="text" name="name" placeholder="Rafi Priatna" class="w-full focus:outline-none text-base font-light" value="{{old('name')}}" autocomplete required />
                        </div>
                    </div>
                    <div class="mt-4">
                        <label class="block text-lg font-medium text-label">Kata Sandi</label>
                        <div class="flex items-center w-full px-5 py-4 mt-3 text-base font-light rounded-xl input">
                            <svg class="mr-4 text-gray-400 w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
                            </svg>
                            <input type="password" name="password" id="password-content-3-5" placeholder="••••••••" minlength="8" class="w-full focus:outline-none text-base font-light" required />
                        </div>
                    </div>
                    <div class="mt-4">
                        <label class="block text-lg font-medium text-label">Konfirmasi Kata Sandi</label>
                        <div class="flex items-center w-full px-5 py-4 mt-3 text-base font-light rounded-xl input">
                            <svg class="mr-4 text-gray-400 w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
                            </svg>
                            <input type="password" name="konfirmasi_password" id="password-content-3-5" placeholder="••••••••" minlength="8" class="w-full focus:outline-none text-base font-light" required />
                        </div>
                    </div>
                    <button type="submit" class="my-9 justify-start text-white font-medium text-xl px-5 py-3 rounded-lg flex bg-champ-green bg-champ-green transition ease-out duration-200 hover:bg-opacity-80">
                        Daftar
                    </button>
                </form>
                <p class="mt-8 text-center text-sm text-foot">
                    Sudah punya akun?
                    <a href="{{ route('masuk') }}" class="font-medium hover:underline text-link">Masuk di sini</a>.
                </p>
            </div>
        </div>
    </div>
</section>
@endsection