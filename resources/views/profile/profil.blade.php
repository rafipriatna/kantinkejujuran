@extends('layout.app')
@section('title', $title)

@section('content')
    <section class="lg:pt-20 pt-10 mb-10">
        <div class="mb-10 text-5xl font-bold leading-tight text-center heading lg:leading-snug font-display">
            {{ $title }}
        </div>
    </section>

    @if (\Session::has('success'))
        <div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800"
            role="alert">
            {!! \Session::get('success') !!}
        </div>
    @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <section>
        <p class="text-red-400 mb-5">
            <code>Silakan isikan kata sandi lama dan kata sandi baru jika ingin mengganti kata sandi.</code>
        </p>

        <form action="{{ route('simpanProfil') }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="grid gap-6 mb-6 lg:grid-cols-2">
                <div>
                    <label class="block mb-2 text-sm font-medium text-gray-900">Student ID</label>
                    <input type="text"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        value="{{ Auth::user()->student_id }}" readonly>
                </div>
                <div>
                    <label class="block mb-2 text-sm font-medium text-gray-900">Nama Lengkap</label>
                    <input type="text" name="name"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        placeholder="Rafi Priatna K" value="{{ Auth::user()->name }}" required>
                </div>
                <div>
                    <label class="block mb-2 text-sm font-medium text-gray-900">Kata Sandi Lama</label>
                    <input type="password" name="password_lama"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                </div>
                <div>
                    <label class="block mb-2 text-sm font-medium text-gray-900">Kata Sandi Baru</label>
                    <input type="password" name="password"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
                </div>
            </div>

            <button
                class="float-right text-white font-medium text-lg px-3 py-3 rounded-lg flex bg-champ-green bg-champ-green transition ease-out duration-200 hover:bg-opacity-80">
                Simpan
            </button>

        </form>
    </section>


    <section>
        <h1 class="text-3xl py-4 border-b mt-20 mb-10">Penjualan Anda</h1>
        <div class="overflow-x-auto bg-white rounded-lg shadow overflow-y-auto relative" style="height: 405px;">
            <table class="border-collapse table-auto w-full whitespace-no-wrap bg-white table-striped relative">
                <thead>
                    <tr class="text-left">
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            #
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Pembeli
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Nama Produk
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Jumlah
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Total Harga
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Tanggal
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($transactions) > 0)
                        @foreach ($transactions as $index => $transaction)
                            <tr class="hover:bg-gray-100">
                                <td class="border-dashed border-t border-gray-200">
                                    <span class="text-gray-700 px-6 py-3 flex items-center">{{ ++$index }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->buyer->name }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->product->product_name }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->quantity }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->total_price }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->date_transaction }}</span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="border-dashed border-t border-gray-200 text-center py-10" colspan="6">
                                Belum ada data
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </section>

@endsection
