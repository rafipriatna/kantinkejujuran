@extends('layout.app')
@section('title', $title)

@section('content')
    <section class="lg:pt-20 pt-10 mb-10">
        <div class="mb-10 text-5xl font-bold leading-tight text-center heading lg:leading-snug font-display">
            {{ $title }}
        </div>
    </section>

    @if (\Session::has('success'))
        <div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800"
            role="alert">
            {!! \Session::get('success') !!}
        </div>
    @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <section>
        <form class="mt-6" action="{{ route('prosesUbahProduk', $product->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="grid mb-6 lg:grid-cols-3 grid-rows">
                <div class="lg:row-span-2">
                    <div class="mb-4">
                        <label class="block mb-2 text-sm font-medium text-gray-900">Foto Produk</label>

                        <input type="file" class="text-gray-900 text-sm w-full py-2.5" name="product_image"
                            id="img-file">

                        <img class="object-contain h-48 w-48"
                            src="{{ Storage::url('produk/') . $product->product_image }}" id="img-preview">
                    </div>
                </div>
                <div class="lg:col-start-2 lg:col-span-2">
                    <div class="grid gap-6 mb-6 lg:grid-cols-2">
                        <div>
                            <div class="mb-4">
                                <label class="block mb-2 text-sm font-medium text-gray-900">Nama Produk</label>
                                <input type="text"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                    name="product_name" placeholder="Cokolatos" value="{{ $product->product_name }}"
                                    required>
                            </div>
                            <div>
                                <label class="block mb-2 text-sm font-medium text-gray-900">Kategori Produk</label>
                                <select name="product_category"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                    name="product_name">
                                    <option {{ $product->product_category == 'Makanan' ? 'selected' : '' }}
                                        value="Makanan">Makanan
                                    </option>
                                    <option {{ $product->product_category == 'Minuman' ? 'selected' : '' }}
                                        value="Minuman">Minuman
                                    </option>
                                    <option {{ $product->product_category == 'Alat Tulis' ? 'selected' : '' }}
                                        value="Alat Tulis">Alat
                                        Tulis
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div>
                            <div class="mb-4">
                                <label class="block mb-2 text-sm font-medium text-gray-900">Harga Produk</label>
                                <input type="number"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                    name="product_price" placeholder="10000" value="{{ $product->product_price }}"
                                    required>
                            </div>
                            <div class="mb-4">
                                <label class="block mb-2 text-sm font-medium text-gray-900">Stok Produk</label>
                                <input type="number"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                    name="product_stock" placeholder="20" value="{{ $product->product_stock }}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lg:col-start-2 lg:col-span-2">
                    <label class="block mb-2 text-sm font-medium text-gray-900">Deskripsi Produk</label>
                    <textarea name="product_description" cols="30" rows="10"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">{{ $product->product_description }}</textarea>
                    <button type="submit"
                        class="mt-10 float-right text-white font-medium text-lg px-3 py-3 rounded-lg flex bg-champ-green bg-champ-green transition ease-out duration-200 hover:bg-opacity-80">
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </section>
@endsection

@section('js')
    <script>
        let originalFileSrc = "";

        $('#img-file').change(function() {
            const input = this;
            const url = $(this).val();
            originalFileSrc = $('#img-preview').attr('src');
            const ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                const reader = new FileReader();

                reader.onload = function(e) {
                    $('#img-preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#img-preview').attr('src', originalFileSrc);
            }
        })
    </script>
@endsection
