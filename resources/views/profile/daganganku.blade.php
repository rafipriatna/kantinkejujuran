@extends('layout.app')
@section('title', $title)

@section('content')
    <section class="lg:pt-20 pt-10 mb-10">
        <div class="mb-10 text-5xl font-bold leading-tight text-center heading lg:leading-snug font-display">
            {{ $title }}
        </div>
    </section>

    @if (\Session::has('success'))
        <div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800"
            role="alert">
            {!! \Session::get('success') !!}
        </div>
    @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <section>
        <div class="container mx-auto py-6 px-4" x-data="datatables()" x-cloak>
            <div class="mb-4 flex justify-between items-center">
                <div class="flex-1 pr-4">
                    <div class="relative md:w-1/3">
                        <input type="search"
                            class="w-full pl-10 pr-4 py-2 rounded-lg shadow focus:outline-none focus:shadow-outline text-gray-600 font-medium"
                            placeholder="Search..." x-model="cariData">
                        <div class="absolute top-0 left-0 inline-flex items-center p-2">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-gray-400" viewBox="0 0 24 24"
                                stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                                stroke-linejoin="round">
                                <rect x="0" y="0" width="24" height="24" stroke="none"></rect>
                                <circle cx="10" cy="10" r="7" />
                                <line x1="21" y1="21" x2="15" y2="15" />
                            </svg>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="shadow rounded-lg flex">
                        <div class="relative">
                            <a href="{{ route('tambahProduk') }}"
                                class="rounded-lg inline-flex items-center hover:bg-champ-green text-champ-green focus:outline-none focus:shadow-outline hover:text-white font-semibold py-2 px-2 md:px-4">
                                Tambah Produk
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="overflow-x-auto bg-white rounded-lg shadow overflow-y-auto relative" style="height: 405px;">
                <table class="border-collapse table-auto w-full whitespace-no-wrap bg-white table-striped relative">
                    <thead>
                        <tr class="text-left">
                            <template x-for="heading in headings">
                                <th class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs"
                                    x-text="heading.value" :x-ref="heading.key"
                                    :class="{
                                        [heading.key]: true
                                    }">
                                </th>
                            </template>
                        </tr>
                    </thead>
                    <tbody>
                        <template x-for="(product, index) in cariProduk" :key="product.id">
                            <tr class="hover:bg-gray-100">
                                <td class="border-dashed border-t border-gray-200">
                                    <span class="text-gray-700 px-6 py-3 flex items-center" x-text="++index"></span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <img class="object-cover h-20 w-20 rounded my-2"
                                        :src="`{{ Storage::url('produk/') }}${product.product_image}`">
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span class="text-gray-700 px-6 py-3 flex items-center"
                                        x-text="product.product_name"></span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span class="text-gray-700 px-6 py-3 flex items-center"
                                        x-text="product.product_category"></span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span class="text-gray-700 px-6 py-3 flex items-center"
                                        x-text="product.product_price"></span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span class="text-gray-700 px-6 py-3 flex items-center"
                                        x-text="product.product_stock"></span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <div class="inline-flex rounded-md shadow-sm" role="group">
                                        <form :action="`/profil/daganganku/${product.id}`" method="post" onsubmit="return confirm('Apakah kamu yakin ingin menghapus produk ini?');">
                                            @csrf
                                            @method('DELETE')
                                            <a :href="`/profil/daganganku/${product.id}/ubah`"
                                                class="py-2 px-4 text-sm font-medium text-gray-900 bg-blue-400 border border-blue-400 rounded-l-md hover:bg-blue-600">
                                                Ubah
                                            </a>
                                            <button
                                                class="py-2 px-4 text-sm font-medium text-gray-900 bg-red-400 rounded-r-md hover:bg-red-600">
                                                Hapus
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </template>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        const products = JSON.parse({{ Js::from($JSONProducts) }})

        function datatables() {
            return {
                cariData: '',
                headings: [{
                        'key': 'nomor',
                        'value': '#'
                    },
                    {
                        'key': 'product_image',
                        'value': 'Foto'
                    },
                    {
                        'key': 'product_name',
                        'value': 'Nama'
                    },
                    {
                        'key': 'product_category',
                        'value': 'Kategori'
                    },
                    {
                        'key': 'product_price',
                        'value': 'Harga'
                    },
                    {
                        'key': 'product_stock',
                        'value': 'Stok'
                    },
                    {
                        'key': 'opsi',
                        'value': ''
                    }
                ],
                products,
                get cariProduk() {
                    return this.products.filter(
                        i => i.product_name.toLowerCase().includes(this.cariData.toLowerCase()) || i
                        .product_category.toLowerCase().includes(this.cariData.toLowerCase())
                    )
                }
            }
        }
    </script>
@endsection
