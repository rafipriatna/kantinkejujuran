@extends('layout.app')
@section('title', $title)

@section('content')
    <section class="lg:pt-20 pt-10 mb-10">
        <div class="mb-10 text-5xl font-bold leading-tight text-center heading lg:leading-snug font-display">
            {{ $title }}
        </div>
    </section>

    @if (\Session::has('success'))
        <div class="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800"
            role="alert">
            {!! \Session::get('success') !!}
        </div>
    @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <table class="my-5">
        <tr>
            <th>
                Tota Uang:
            </th>
            <td>
                <span>{{ $balanceBoxAmount }}</span>
            </td>
        </tr>
    </table>

    <section>
        <h1 class="text-2xl mb-5">Deposit</h1>
        <form action="{{ route('balanceBoxDeposit') }}" method="post">
            @csrf
            <div class="grid gap-6 mb-6 lg:grid-cols-2">
                <div>
                    <input type="number"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        placeholder="10000" name="amount" required>
                </div>
                <div>
                    <button type="submit"
                        class="bg-champ-green w-sm text-white font-bold py-2 px-4 rounded inline-flex items-center justify-center transition duration-500 hover:bg-opacity-80">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        <span>Proses</span>
                    </button>
                </div>
            </div>
        </form>
    </section>

    <section>
        <h1 class="text-2xl mb-5">Withdraw</h1>
        <form action="{{ route('balanceBoxDeposit') }}" method="post">
            @csrf
            @method('DELETE')
            <div class="grid gap-6 mb-6 lg:grid-cols-2">
                <div>
                    <input type="number"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                        placeholder="10000" name="amount" required>
                </div>
                <div>
                    <button type="submit"
                        class="bg-champ-green w-sm text-white font-bold py-2 px-4 rounded inline-flex items-center justify-center transition duration-500 hover:bg-opacity-80">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        <span>Proses</span>
                    </button>
                </div>
            </div>
        </form>
    </section>

    <section>
        <h1 class="text-3xl py-4 border-b mb-10">Riwayat Transaksi</h1>
        <div class="overflow-x-auto bg-white rounded-lg shadow overflow-y-auto relative" style="height: 405px;">
            <table class="border-collapse table-auto w-full whitespace-no-wrap bg-white table-striped relative">
                <thead>
                    <tr class="text-left">
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            #
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Penjual
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Nama Produk
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Jumlah
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Total Harga
                        </th>
                        <th
                            class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                            Tanggal
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($transactions) > 0)
                        @foreach ($transactions as $index => $transaction)
                            <tr class="hover:bg-gray-100">
                                <td class="border-dashed border-t border-gray-200">
                                    <span class="text-gray-700 px-6 py-3 flex items-center">{{ ++$index }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->seller->name }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->product->product_name }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->quantity }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->total_price }}</span>
                                </td>
                                <td class="border-dashed border-t border-gray-200">
                                    <span
                                        class="text-gray-700 px-6 py-3 flex items-center">{{ $transaction->date_transaction }}</span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="border-dashed border-t border-gray-200 text-center py-10" colspan="6">
                                Belum ada data
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </section>

@endsection
