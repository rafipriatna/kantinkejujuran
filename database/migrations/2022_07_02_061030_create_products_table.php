<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('student_id');
            $table->string('product_image');
            $table->string('product_name');
            $table->string('product_slug');
            $table->text('product_description');
            $table->enum('product_category', ['Makanan', 'Minuman', 'Alat Tulis']);
            $table->integer('product_price');
            $table->integer('product_stock');
            $table->timestamps();

            $table->foreign('student_id')->references('student_id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
