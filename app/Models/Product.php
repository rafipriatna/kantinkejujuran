<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'product_image',
        'product_name',
        'product_slug',
        'product_category',
        'product_price',
        'product_stock',
        'product_description'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
