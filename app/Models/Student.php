<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    use HasFactory;
    protected $primaryKey = "student_id";
    public $incrementing = false;

    protected $fillable = [
        'student_id',
        'name',
        'password',
    ];

    protected $hidden = [
        'password',
    ];

    // Canteen Balance Box
    public function CanteenBalanceBox()
    {
        return CanteenBalanceBox::where('id', 1)->first();
    }

    public function getCanteenBalanceBoxAttribute()
    {
        return rupiah($this->CanteenBalanceBox()->amount ?? 0);
    }
}
