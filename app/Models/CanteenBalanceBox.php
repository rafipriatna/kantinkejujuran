<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CanteenBalanceBox extends Model
{
    use HasFactory;
    
    protected $table = 'canteen_balance_box';

    protected $fillable = [
        'amount'
    ];
}
