<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

class ProductCategoryEnum extends Enum
{
    const MAKANAN = 'Makanan';
    const MINUMAN = 'Minuman';
    const ALATTULIS = 'Alat Tulis';
    public static $types = [self::MAKANAN, self::MINUMAN, self::ALATTULIS];
}
