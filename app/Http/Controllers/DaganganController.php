<?php

namespace App\Http\Controllers;

use App\Http\Requests\BeliDaganganRequest;
use App\Models\CanteenBalanceBox;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

setlocale(LC_MONETARY, "id_ID"); // Untuk mata uang

class DaganganController extends Controller
{
    public function index(Request $request)
    {
        $sortBy = 'terbaru';
        $sortCategory = 'semua';
        $orderByquery = $request->query('sort_by');
        $categoryQuery = $request->query('category');
        $sort = 'ASC';
        $orderBy = 'created_at';
        $whereClause = [['product_stock', '>', 0]];

        $sortByURLPath = '?sort_by=terbaru';
        $cateogoryURLPath = '';

        if ($categoryQuery) {
            $sortCategory = $categoryQuery;
            $categoryName = '';
            if ($categoryQuery == 'makanan') {
                $categoryName = 'Makanan';
            } else if ($categoryQuery == 'minuman') {
                $categoryName = 'Minuman';
            } else if ($categoryQuery == 'alat-tulis') {
                $categoryName = 'Alat Tulis';
            }
            $cateogoryURLPath .= '&category=' . $categoryQuery;
            array_push($whereClause, ['product_category', '=', $categoryName]);
        }

        if ($orderByquery) {
            $sortBy = $orderByquery;
            if ($orderByquery == 'terlama') {
                $orderBy = 'created_at';
                $sort = 'ASC';
            } else if ($orderByquery == 'terendah') {
                $orderBy = 'product_price';
                $sort = 'ASC';
            } else if ($orderByquery == 'tertinggi') {
                $orderBy = 'product_price';
                $sort = 'DESC';
            } else if ($orderByquery == 'a-z') {
                $orderBy = 'product_name';
                $sort = 'ASC';
            } else if ($orderByquery == 'z-a') {
                $orderBy = 'product_name';
                $sort = 'DESC';
            } else {
                $sortBy = 'terbaru';
                $orderBy = 'created_at';
                $sort = 'DESC';
            }
            $sortByURLPath = '?sort_by=' . $sortBy;
        } else {
            $orderBy = 'created_at';
            $sort = 'DESC';
        }

        $products = Product::with('student')->where($whereClause)->orderBy($orderBy, $sort)->get();

        $productSorting = [
            'terbaru' => ['Paling Baru', route('dagangan') . '?sort_by=terbaru' . $cateogoryURLPath],
            'terlama' => ['Paling Lama', route('dagangan') . '?sort_by=terlama' . $cateogoryURLPath],
            'terendah' => ['Harga Terendah', route('dagangan') . '?sort_by=terendah' . $cateogoryURLPath],
            'tertinggi' => ['Harga Tertinggi', route('dagangan') . '?sort_by=tertinggi' . $cateogoryURLPath],
            'a-z' => ['Urutkan A ke Z', route('dagangan') . '?sort_by=a-z' . $cateogoryURLPath],
            'z-a' => ['Urutkan Z ke A', route('dagangan') . '?sort_by=z-a' . $cateogoryURLPath],
        ];

        $categoriesSorting = [
            'semua' => ['Semua Kategori', route('dagangan') . $sortByURLPath],
            'makanan' => ['Makanan', route('dagangan') . $sortByURLPath . '&category=makanan'],
            'minuman' => ['Minuman', route('dagangan') . $sortByURLPath . '&category=minuman'],
            'alat-tulis' => ['Alat Tulis', route('dagangan') . $sortByURLPath . '&category=alat-tulis']
        ];

        foreach ($products as $product) :
            $product['product_price'] = rupiah($product['product_price']);
        endforeach;

        $data = [
            'title' => 'Dagangan',
            'products' => $products,
            'sortBy' => $sortBy,
            'sortCategory' => $sortCategory,
            'productSorting' => $productSorting,
            'categoriesSorting' => $categoriesSorting
        ];

        return view('dagangan.dagangan', $data);
    }

    public function detail($slug)
    {
        $product = Product::where('product_slug', $slug)->firstOrFail();
        $product['product_price'] = rupiah($product['product_price']);

        $data = [
            'title' => $product['product_name'],
            'product' => $product
        ];

        return view('dagangan.detail', $data);
    }

    public function beli($slug)
    {
        $product = Product::where('product_slug', $slug)->firstOrFail();
        $product['product_price'] = rupiah($product['product_price']);

        $data = [
            'title' => "Beli " . $product['product_name'],
            'product' => $product
        ];

        return view('dagangan.beli', $data);
    }

    public function prosesBeliDagangan(BeliDaganganRequest $request, $id)
    {
        try {
            $studentLoggedIn = Auth::user();

            $quantity = intval($request->input('quantity'));
            $product = Product::where('id', $id)->first();
            $sellerStudentId = $product->student_id;
            $buyerStudentId = $studentLoggedIn->student_id;
            $jumlahBayar = intval($request->input('paid'));

            // Hitung harga dan kurangi stock
            $totalHarga = $product->product_price * $quantity;

            if ($product->product_stock < $quantity) {
                return response()->json(
                    [
                        'title' => 'Transaksi gagal',
                        'message' => 'Stok produk tidak mencukupi.'
                    ],
                    200
                );
            }

            // Tambah jumlah yang dibayarkan ke balance canteen balance box
            // Apakah perlu validasi totalHarga ke harga yang dibayarkan? 
            // Sepertinya tidak, karna ini kantin kejujuran.
            // Tentu saja untuk menguji kejujuran (?)
            $balanceBox = CanteenBalanceBox::firstOrNew(['id' => 1]);
            $balanceBox->amount = ($balanceBox->amount + $jumlahBayar);
            $balanceBox->save();

            // Kurangi stok
            Product::where('id', $id)->decrement('product_stock', $quantity);

            Transaction::create([
                'seller_student_id' => $sellerStudentId,
                'buyer_student_id' => $buyerStudentId,
                'product_id' => $id,
                'quantity' => $quantity,
                'total_price' => $totalHarga
            ]);

            return response()->json(
                [
                    'title' => 'Berhasil',
                    'message' => 'Pembelian berhasil! Silakan ambil produk yang Anda beli.'
                ],
                200
            );
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
