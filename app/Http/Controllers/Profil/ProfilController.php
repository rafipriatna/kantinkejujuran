<?php

namespace App\Http\Controllers\Profil;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\SaldoRequest;
use App\Models\HistoryBalance;
use App\Models\Student;
use App\Models\Transaction;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{
    public function index()
    {
        $transactions = Transaction::with('product', 'buyer')->where('seller_student_id', Auth::user()->student_id)->get();

        foreach ($transactions as $transaction) {
            $transaction['total_price'] = rupiah($transaction['total_price']);
            $transaction['date_transaction'] = Carbon::parse($transaction['created_at'])->translatedFormat('d F Y');
        }

        $data = [
            'title' => 'Profil',
            'transactions' => $transactions
        ];

        return view('profile.profil', $data);
    }

    public function simpanProfil(ProfileRequest $request)
    {
        try {
            $input = $request->only('name', 'password', 'password_lama');

            if ($input['password'] && $input['password_lama']) {
                $student = Auth::user();

                if (!Hash::check($input['password_lama'], $student->password)) {
                    return redirect()->route('profil')
                        ->withErrors(['error' => 'Kata sandi lama tidak cocok.']);
                }

                $input['password'] = Hash::make($input['password']);
            }

            unset($input['password_lama']);

            Student::where('student_id', Auth::user()->student_id)->update($input);

            return redirect()->route('profil')->with('success', 'Berhasil memperbarui profil.');
        } catch (\Exception $e) {
            return redirect()->route('profil')
                ->withInput()
                ->withErrors(['error' => $e->getMessage()]);
        }
    }
}
