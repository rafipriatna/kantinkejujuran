<?php

namespace App\Http\Controllers\Profil;

use App\Http\Controllers\Controller;
use App\Http\Requests\Produk\TambahProdukRequest;
use App\Http\Requests\Produk\UbahProdukRequest;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class DagangankuController extends Controller
{
    public function index()
    {
        $products = Product::where('student_id', Auth::user()->student_id)->get();

        foreach($products as $product):
            $product['product_price'] = rupiah($product['product_price']);
        endforeach;
        
        $JSONProducts = json_encode($products);

        $data = [
            'title' => 'DaganganKu',
            'JSONProducts' => $JSONProducts
        ];

        return view('profile.daganganku', $data);
    }

    public function tambahProduk()
    {
        $data = [
            'title' => 'Tambah Produk'
        ];

        return view('profile.tambah_produk', $data);
    }

    public function ubahProduk($id)
    {
        $product = Product::where('id', $id)->firstOrFail();
        $data = [
            'title' => 'Ubah Produk',
            'product' => $product
        ];

        return view('profile.ubah_produk', $data);
    }

    public function prosesTambahProduk(TambahProdukRequest $request)
    {
        try {
            $input = $request->only('product_name', 'product_description', 'product_price', 'product_category', 'product_stock');

            $path = storage_path('app/public/produk/');
            $file = $request->file('product_image');
            $name = uniqid() . '_' . trim($file->getClientOriginalName());
            $file->move($path, $name);

            $input['student_id'] = Auth::user()->student_id;
            $input['product_image'] = $name;
            $input['product_slug'] = Str::slug($input['product_name'], '-');

            Product::create($input);
            return redirect()->route('daganganku')->with('success', 'Berhasil menambahkan produk.');
        } catch (\Exception $e) {
            return redirect()->route('tambahProduk')
                ->withInput()
                ->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function prosesUbahProduk(UbahProdukRequest $request, $id)
    {
        try {
            $input = $request->only('product_name', 'product_description', 'product_price', 'product_category', 'product_stock');
            $input['student_id'] = Auth::user()->student_id;

            if ($request->hasFile('product_image')) {
                $path = storage_path('app/public/produk/');
                $file = $request->file('product_image');
                $name = uniqid() . '_' . trim($file->getClientOriginalName());
                $file->move($path, $name);

                $input['product_image'] = $name;
            }

            $input['product_slug'] = Str::slug($input['product_name'], '-');

            Product::where('id', $id)->update($input);
            return redirect()->route('daganganku')->with('success', 'Berhasil mengubah produk.');
        } catch (\Exception $e) {
            return redirect()->route('ubahProduk', $id)
                ->withInput()
                ->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function prosesHapusProduk($id)
    {
        try {
            Product::where('id', $id)->delete();
            return redirect()->route('daganganku')->with('success', 'Berhasil menghapus produk.');
        } catch (\Exception $e) {
            return redirect()->route('daganganku')
                ->withInput()
                ->withErrors(['error' => $e->getMessage()]);
        }
    }
}
