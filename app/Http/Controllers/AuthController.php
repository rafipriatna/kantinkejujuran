<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\Student;

use App\Http\Requests\DaftarRequest;

class AuthController extends Controller
{
    // Pages
    public function masuk()
    {
        $data = [
            'title' => 'Masuk'
        ];

        return view('auth.masuk', $data);
    }

    public function daftar()
    {
        $data = [
            'title' => 'Daftar'
        ];

        return view('auth.daftar', $data);
    }

    // Action
    public function prosesMasuk(Request $request)
    {
        $input = $request->only('student_id', 'password');
        if (Auth::attempt($input)) {
            Auth::user();
            return Redirect('/');
        }

        return redirect('masuk')
            ->withInput()
            ->withErrors(['error' => 'Student ID atau kata sandi salah.']);
    }

    public function prosesDaftar(DaftarRequest $request)
    {
        $input = $request->only('student_id', 'name', 'password', 'konfirmasi_password');

        $validasiStudentId = $this->validasiStudentId($input['student_id']);

        if ($validasiStudentId) {

            $validasiPassword = $this->validasiPassword($input['password'], $input['konfirmasi_password']);

            if ($validasiPassword) {
                $input['password'] = Hash::make($input['password']);
                Student::create($input);
                return redirect('masuk')->with('success', 'Berhasil mendaftar. Silakan masuk.');
            }

            return redirect('daftar')
                ->withInput()
                ->withErrors(['error' => 'Konfirmasi kata sandi tidak cocok.']);
        }

        return redirect('daftar')
            ->withInput()
            ->withErrors(['error' => 'Student ID salah.']);
    }

    public function validasiStudentId($id)
    {
        $threeFirstDigits = str_split(substr($id, 0, 3));
        $twoLastDigits = substr($id, 3, 2);

        $sumFirstDigit = 0;
        foreach ($threeFirstDigits as $digit) :
            $sumFirstDigit += $digit;
        endforeach;

        $jawaban = str_pad($sumFirstDigit, 2, '0', STR_PAD_LEFT);

        return $jawaban == $twoLastDigits;
    }

    public function validasiPassword($password1, $password2)
    {
        return $password1 == $password2;
    }

    public function keluar()
    {
        Auth::logout();
        return Redirect('/');
    }
}
