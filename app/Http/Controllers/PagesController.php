<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function beranda() {
        $data = [
            'title' => 'Beranda'
        ];
        return view('beranda', $data);
    }

    public function tentang() {
        $data = [
            'title' => 'Tentang'
        ];
        return view('tentang', $data);
    }
}
