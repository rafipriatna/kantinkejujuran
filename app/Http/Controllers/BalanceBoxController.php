<?php

namespace App\Http\Controllers;

use App\Http\Requests\BalanceRequest;
use App\Models\CanteenBalanceBox;
use App\Models\Transaction;
use Illuminate\Support\Carbon;

class BalanceBoxController extends Controller
{
    public function index()
    {
        $transactions = Transaction::with('product', 'seller')->get();

        foreach ($transactions as $transaction) {
            $transaction['total_price'] = rupiah($transaction['total_price']);
            $transaction['date_transaction'] = Carbon::parse($transaction['created_at'])->translatedFormat('d F Y');
        }
        $balanceBoxAmount = rupiah(CanteenBalanceBox::where('id', 1)->first()->amount ?? 0);

        $data = [
            'title' => 'Balance Box',
            'transactions' => $transactions,
            'balanceBoxAmount' => $balanceBoxAmount
        ];

        return view('balance_box', $data);
    }

    public function deposit(BalanceRequest $request)
    {
        try {
            $amount = $request->input('amount');
            $balanceBox = CanteenBalanceBox::firstOrNew(['id' => 1]);
            $balanceBox->amount = ($balanceBox->amount + $amount);
            $balanceBox->save();

            return redirect()->route('balanceBox')->with('success', 'Berhasil menambahkan uang.');
        } catch (\Exception $e) {
            return redirect()->route('balanceBox')
                ->withInput()
                ->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function withdraw(BalanceRequest $request)
    {
        try {
            $amount = $request->input('amount');
            $balanceBox = CanteenBalanceBox::firstOrNew(['id' => 1]);

            if ($balanceBox->amount < $amount) {
                return redirect()->route('balanceBox')->withErrors(['error' => 'Jumlah withdraw melebihi jumlah uang yang tersedia.']);
            }
            
            $balanceBox->amount = ($balanceBox->amount - $amount);
            $balanceBox->save();

            return redirect()->route('balanceBox')->with('success', 'Berhasil menarik uang.');
        } catch (\Exception $e) {
            return redirect()->route('balanceBox')
                ->withErrors(['error' => $e->getMessage()]);
        }
    }
}
