<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DaftarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "student_id" => "required|numeric|digits:5|unique:students",
            "name" => "required",
            "password" => "required|min:8",
            "konfirmasi_password" => "required",
        ];
    }

    public function messages()
    {
        return [
            "student_id.required" => "Isian Student ID wajib diisi.",
            "student_id.numeric" => "Isian Student ID harus berisi angka",
            "student_id.digits" => "Isian Student ID harus berisi 5 digit angka.",
            "student_id.unique" => "Akun dengan Student ID " . $this->get("student_id") . " sudah terdaftar.",
            "name.required" => "Isian nama harus diisi.",
            "password.required" => "Isian kata sandi wajib diisi.",
            "password.min" => "Isian kata sandi minimal mengandung 8 karakter.",
            "konfirmasi_password.required" => "Konfirmasi kata sandi wajib diisi.",
        ];
    }
}
