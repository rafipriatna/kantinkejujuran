<?php

namespace App\Http\Requests\Produk;

use App\Enums\ProductCategoryEnum;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class TambahProdukRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "product_image" => "required|image|mimes:jpeg,png,jpg|max:1024",
            "product_name" => "required",
            "product_description" => "required",
            "product_category" => Rule::in(ProductCategoryEnum::$types),
            "product_price" => "required|numeric",
            "product_stock" => "required|numeric",
        ];
    }

    public function messages()
    {
        return [
            "product_image.required" => "Harus menyertakan foto produk.",
            "product_image.image" => "Foto produk harus berformat gambar.",
            "product_image.max" => "Ukuran terlalu besar! Maksimal ukuran foto produk adalah 1MB.",
            "product_image.mimes" => "Foto produk harus berekstensi jpeg, png, atau jpg.",
            "product_name.required" => "Harus menyertakan nama produk.",
            "product_description.required" => "Harus menyertakan deskripsi produk.",
            "product_category.rule" => "Harus menyertakan kategori produk.",
            "product_price.required" => "Harus menyertakan harga produk.",
            "product_price.numeric" => "Harga produk harus angka.",
            "product_stock.required" => "Harus menyertakan stok produk.",
            "product_stock.numeric" => "Stok produk harus angka.",
        ];
    }
}
