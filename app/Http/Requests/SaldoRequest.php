<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class saldoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "amount" => "required|numeric|max:99999999",
        ];
    }

    public function messages()
    {
        return [
            "amount.required" => "Jumlah uang harus diisi.",
            "amount.numeric" => "Jumlah uang harus berbentuk angka.",
            "amount.max" => "Jumlah uang terlalu banyak.",
        ];
    }
}
