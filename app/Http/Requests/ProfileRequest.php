<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            "password_lama" => "min:8",
            "password" => "min:8",
        ];
    }

    public function messages()
    {
        return [
            "name.required" => "Isian nama harus diisi.",
            "password_lama.min" => "Isian kata sandi minimal mengandung 8 karakter.",
            "password.min" => "Isian kata sandi baru minimal mengandung 8 karakter.",
        ];
    }
}
