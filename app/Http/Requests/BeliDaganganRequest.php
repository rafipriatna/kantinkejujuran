<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BeliDaganganRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'quantity' => 'required|numeric',
            'paid' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'quantity.required' => 'Quantity wajib diisi.',
            'quantity.numeric' => 'Quantity wajib angka.',
            'paid.required' => 'Jumlah bayar wajib diisi.',
            'paid.numeric' => 'Jumlah bayar wajib angka.',
        ];
    }
}
