<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BalanceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'amount.required' => 'Jumlah uang harus diisi.',
            'amount.numeric' => 'Jumlah uang harus angka.',
        ];
    }
}
