<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MasukRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "student_id" => "required|numeric|digits:5",
            "password" => "required",
        ];
    }

    public function messages()
    {
        return [
            "student_id.required" => "Isian Student ID wajib diisi.",
            "student_id.numeric" => "Isian Student ID harus berisi angka",
            "student_id.digits" => "Isian Student ID harus berisi 5 digit angka.",
            "password.required" => "Isian kata sandi wajib diisi."
        ];
    }
}
