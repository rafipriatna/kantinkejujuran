<?php

namespace Tests\Feature;

use App\Models\Student;
use Tests\TestCase;

class BalanceBoxTest extends TestCase
{
    /**
     * @test
     * Non Student can not access balance-box
     */
    public function test_non_student_can_not_access_balance_box()
    {
        $response = $this->get(route('balanceBox'));
        $response->assertStatus(302);
        $response->assertRedirect(route('masuk'));
    }

    /**
     * @test
     * Student can access balance-box
     */
    public function test_student_can_access_balance_box()
    {
        $student = Student::factory()->create();
        $this->be($student);

        $response = $this->get(route('balanceBox'));
        $response->assertOk();
    }

    /**
     * @test
     * Student can deposit and withdraw balance-box
     */
    public function test_student_can_deposit_and_withdraw_balance_box()
    {
        $student = Student::factory()->make();
        $this->be($student);

        $deposit = $this->from(route('balanceBox'))
            ->post(route('balanceBoxDeposit', 1), [
                'amount' => 10000
            ]);
        $deposit->assertStatus(302);
        $deposit->assertRedirect(route('balanceBox'));
        $deposit->assertSessionHasNoErrors();

        $withdraw = $this->from(route('balanceBox'))
            ->post(route('balanceBoxWithdraw', 1), [
                'amount' => 10000
            ]);
        $withdraw->assertStatus(302);
        $withdraw->assertRedirect(route('balanceBox'));
        $deposit->assertSessionHasNoErrors();
    }
}
