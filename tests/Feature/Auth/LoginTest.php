<?php

namespace Tests\Feature\Auth;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * Student access login page.
     * */
    public function test_student_can_access_login_page()
    {
        $response = $this->get('/masuk');
        $response->assertStatus(200);
    }

    /**
     * @test
     * Student can login.
     * */
    public function test_student_can_login()
    {
        Student::factory()->create();

        $session = $this->from(route('masuk'))
            ->post(route('prosesMasuk'), [
                'student_id' => '45615',
                'password' => '12345678',
            ]);
        $session->assertStatus(302);
        $session->assertRedirect(route('beranda'));
    }

    /**
     * @test
     * Student can logout.
     * */
    public function test_student_can_logout()
    {
        $student = Student::factory()->make();
        $this->be($student);

        $this->get(route('keluar'))
            ->assertRedirect(route('beranda'));
        $this->assertGuest();
    }

    /**
     * @test
     * Student can not login if already logged in.
     * */
    public function test_student_can_not_login_if_already_logged_in()
    {
        $student = Student::factory()->make();
        $this->be($student);

        $this->get(route('masuk'))
            ->assertStatus(302)
            ->assertRedirect(route('beranda'));
        $this->assertAuthenticated();
    }

    /**
     * @test
     * Student can not login to app.
     * with invalid student_id
     * */
    public function test_student_can_not_login_invalid_student_id()
    {
        $response = $this->from(route('masuk'))
            ->post(route('prosesMasuk'), [
                'student_id' => '45614',
                'password' => '12345678',
            ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('masuk'));

        $response->assertSessionHasErrors();
    }

    /**
     * @test
     * Student can not login to app.
     * with invalid password
     * */
    public function test_student_can_not_login_invalid_password()
    {
        $response = $this->from(route('masuk'))
            ->post(route('prosesMasuk'), [
                'student_id' => '45615',
                'password' => '12345674',
            ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('masuk'));

        $response->assertSessionHasErrors();
    }
}
