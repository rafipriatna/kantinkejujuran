<?php

namespace Tests\Feature\Auth;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * Student access register page.
     * */
    public function test_student_can_access_register_page()
    {
        $response = $this->get('/daftar');
        $response->assertStatus(200);
    }

    /**
     * @test
     * Student create an account.
     * */
    public function test_student_can_create_an_account()
    {
        Student::factory()->create();
        
        $response = $this->from(route('daftar'))
            ->post(route('prosesDaftar'), [
                'student_id' => '11406',
                'name' => 'Testing Student Lagi',
                'password' => '12345678',
                'konfirmasi_password' => '12345678'
            ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('masuk'));
        $response->assertSessionHasNoErrors();
    }

    /**
     * @test
     * Student cannot create an account.
     * with invalid student_id
     * */
    public function test_student_register_invalid_student_id()
    {
        $response = $this->from(route('daftar'))
            ->post(route('prosesDaftar'), [
                'student_id' => '12305',
                'name' => 'Testing Student',
                'password' => '12345678',
                'konfirmasi_password' => '12345678'
            ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('daftar'));

        $response->assertSessionHasErrors();
    }

    /**
     * @test
     * Student cannot create an account.
     * with invalid password length or confirm password not equal.
     * */
    public function test_student_invalid_password_length()
    {
        $response = $this->from(route('daftar'))
            ->post(route('prosesDaftar'), [
                'student_id' => '12306',
                'name' => 'Testing Student',
                'password' => '123456',
                'konfirmasi_password' => '12345678'
            ]);
        $response->assertStatus(302);
        $response->assertRedirect(route('daftar'));
    }
}
