<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\Student;
use Tests\TestCase;

class DaganganTest extends TestCase
{
    /**
     * @test
     * Student can access dagangan
     */
    public function test_student_can_access_dagangan()
    {
        // Init
        Student::factory()->create(['student_id' => '46515']);
        Product::factory()->create(['student_id' => '46515', 'product_image' => 'test.jpg', 'product_slug' => 'test-produk']);


        $student = Student::factory()->create(['student_id' => '12407']);
        $this->be($student);

        $response = $this->get(route('dagangan'));
        $response->assertOk();
    }

    /**
     * @test
     * Student can access detail dagangan
     */
    public function test_student_can_access_detail_dagangan()
    {
        $student = Student::factory()->make();
        $this->be($student);

        $response = $this->get(route('detailDagangan', 'test-produk'));
        $response->assertOk();
    }

    /**
     * @test
     * Student can access beli dagangan
     */
    public function test_student_can_access_beli_dagangan()
    {
        $student = Student::factory()->make(['student_id' => '12407']);
        $this->be($student);

        $response = $this->get(route('beliDagangan', 'test-produk'));
        $response->assertOk();
    }

    /**
     * @test
     * Non Student can not access beli dagangan
     */
    public function test_non_student_can_not_access_beli_dagangan()
    {
        $response = $this->get(route('beliDagangan', 'test-produk'));
        $response->assertStatus(302);
        $response->assertRedirect(route('masuk'));
    }

    /**
     * @test
     * Student can buy dagangan
     */
    public function test_student_can_buy_dagangan()
    {
        $student = Student::factory()->make(['student_id' => '12407']);
        $this->be($student);

        $response = $this->from(route('beliDagangan', 'test-produk'))
            ->post(route('prosesBeliDagangan', 1), [
                'quantity' => 2,
                'paid' => 10000
            ]);

        $response->assertStatus(200)->assertJson([
            "message" => "Pembelian berhasil! Silakan ambil produk yang Anda beli."
        ]);
    }

    /**
     * @test
     * Student can not buy dagangan
     * with invalid quantity
     */
    public function test_student_can_not_buy_dagangan_invalid_quantity()
    {
        $student = Student::factory()->make(['student_id' => '12407']);
        $this->be($student);

        $response = $this->from(route('beliDagangan', 'test-produk'))
            ->post(route('prosesBeliDagangan', 1), [
                'quantity' => 11,
                'paid' => 10000
            ]);

        $response->assertStatus(200)->assertJson([
            "message" => "Stok produk tidak mencukupi."
        ]);
    }
}
