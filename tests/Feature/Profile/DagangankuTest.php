<?php

namespace Tests\Feature\Profile;

use App\Models\Student;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class DagangankuTest extends TestCase
{
    /**
     * @test
     * Student can access daganganku
     */
    public function test_student_can_access_daganganku()
    {
        $student = Student::factory()->make();
        $this->be($student);

        $response = $this->get(route('daganganku'));
        $response->assertOk();
    }

    /**
     * @test
     * Student can create a product
     */
    public function test_student_can_create_a_product()
    {
        $student = Student::factory()->make();
        $this->be($student);

        $response = $this->from(route('tambahProduk'))
            ->post(route('prosesTambahProduk'), [
                'student_id' => $student->student_id,
                'product_image' => UploadedFile::fake()->create("test.jpg", 100),
                'product_name' => 'Testing Produk',
                'product_category' => 'Alat Tulis',
                'product_description' => 'Testing Product Description',
                'product_price' => 10000,
                'product_stock' => 10
            ]);

        $response->assertStatus(302);
        $response->assertRedirect(route('daganganku'));
    }

    /**
     * @test
     * Student can edit a product
     */
    public function test_student_can_edit_a_product()
    {
        $student = Student::factory()->make();
        $this->be($student);

        $response = $this->from(route('ubahProduk', 1))
            ->patch(route('prosesUbahProduk', 1), [
                'student_id' => $student->student_id,
                'product_image' => UploadedFile::fake()->create("test.jpg", 100),
                'product_name' => 'Testing Ganti Produk',
                'product_category' => 'Alat Tulis',
                'product_description' => 'Testing Product Description',
                'product_price' => 10000,
                'product_stock' => 10
            ]);

        $response->assertStatus(302);
        $response->assertRedirect(route('daganganku'));
    }

    /**
     * @test
     * Student can delete a product
     */
    public function test_student_can_delete_a_product()
    {
        $student = Student::factory()->make();
        $this->be($student);

        $response = $this->from(route('daganganku'))
            ->delete(route('prosesHapusProduk', 1));

        $response->assertStatus(302);
        $response->assertRedirect(route('daganganku'));
    }
}
