<?php

use Illuminate\Support\Facades\Route;

// Controllers
use App\Http\Controllers\PagesController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BalanceBoxController;
use App\Http\Controllers\DaganganController;
use App\Http\Controllers\Profil\ProfilController;
use App\Http\Controllers\Profil\DagangankuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'beranda'])->name('beranda');
Route::get('/tentang', [PagesController::class, 'tentang'])->name('tentang');

// Auth
Route::get('/masuk', [AuthController::class, 'masuk'])->middleware('guest')->name('masuk');
Route::get('/daftar', [AuthController::class, 'daftar'])->middleware('guest')->name('daftar');

Route::post('/daftar', [AuthController::class, 'prosesDaftar'])->name('prosesDaftar');
Route::post('/masuk', [AuthController::class, 'prosesMasuk'])->name('prosesMasuk');

// Profile
Route::middleware(['cek.auth'])->prefix('profil')->group(function () {
    Route::get('/', [ProfilController::class, 'index'])->name('profil');
    Route::patch('/', [ProfilController::class, 'simpanProfil'])->name('simpanProfil');

    // Products
    Route::get('/daganganku', [DagangankuController::class, 'index'])->name('daganganku');
    Route::get('/daganganku/tambah', [DagangankuController::class, 'tambahProduk'])->name('tambahProduk');
    Route::get('/daganganku/{id}/ubah', [DagangankuController::class, 'ubahProduk'])->name('ubahProduk');

    Route::post('/daganganku/tambah', [DagangankuController::class, 'prosesTambahProduk'])->name('prosesTambahProduk');
    Route::patch('/daganganku/{id}', [DagangankuController::class, 'prosesUbahProduk'])->name('prosesUbahProduk');
    Route::delete('/daganganku/{id}', [DagangankuController::class, 'prosesHapusProduk'])->name('prosesHapusProduk');


    Route::get('/keluar', [AuthController::class, 'keluar'])->name('keluar');
});

// Dagangan
Route::prefix('dagangan')->group(function () {
    Route::get('/', [DaganganController::class, 'index'])->name('dagangan');
    Route::get('/{slug}', [DaganganController::class, 'detail'])->name('detailDagangan');

    Route::middleware(['cek.auth'])->get('/{slug}/beli', [DaganganController::class, 'beli'])->name('beliDagangan');
    Route::middleware(['cek.auth'])->post('/{id}/beli', [DaganganController::class, 'prosesBeliDagangan'])->name('prosesBeliDagangan');
});

// Balance Box
Route::middleware(['cek.auth'])->get('balance-box', [BalanceBoxController::class, 'index'])->name('balanceBox');
Route::middleware(['cek.auth'])->post('balance-box', [BalanceBoxController::class, 'deposit'])->name('balanceBoxDeposit');
Route::middleware(['cek.auth'])->delete('balance-box', [BalanceBoxController::class, 'withdraw'])->name('balanceBoxWithdraw');

