/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  theme: {
    extend: {
      colors: {
        'champ-green': '#0DDB93',
        'tile-grey': '#939EA3',
        'border-grey': '#304454',
      },
    }
  },
  plugins: [],
}
